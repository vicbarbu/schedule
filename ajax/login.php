<?php
session_start();

require_once "../bin/DatabaseInfo.php";
require_once "../bin/UAC.php";

if(isset($_POST['email']) && isset($_POST['password'])) {
  $success = Schedule\UAC::login($_POST['email'], $_POST['password']);

  $data = array(
    "success" => $success,
    "status" => 200
  );
  print json_encode($data);
} else {
  $data = array(
    "success" => false,
    "status" => 401
  );
  print json_encode($data);
}