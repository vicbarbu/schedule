<?php

require_once "../bin/DatabaseInfo.php";
require_once "../bin/UserRepository.php";

if(isset($_POST["email"]) && isset($_POST["password1"]) && isset($_POST["password2"])) {
  $repo = new \Schedule\UserRepository();
  $result = $repo->register($_POST["email"], $_POST["password1"], $_POST["password2"]);

  $data = array(
    "success" => $result == \Schedule\RegisterResult::SUCCESS,
    "error" => $result != \Schedule\RegisterResult::SUCCESS ? $result : null,
    "status" => 200
  );

  print json_encode($data);
} else {
  $data = array(
    "success" => false,
    "status" => 401
  );

  print json_encode($data);
}