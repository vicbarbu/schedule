<?php

session_start();
require_once "../bin/DatabaseInfo.php";
require_once "../bin/UAC.php";
require_once "../bin/ScheduleLoader.php";

$user = Schedule\UAC::requireLogin();

if(!$user) {
  $res = array(
    "success" => false,
    "status" => 200,
    "error" => array(
      "code" => "account",
      "text" => "Nu ești autentificat. Pentru a putea accesa orarul trebuie să te autentifici."
    )
  );

  print json_encode($res);
} else if(isset($_POST["data"])) {
  $schedule = new Schedule\ScheduleLoader($user);
  $schedule->save($_POST["data"]);


  $res = array(
    "success" => true,
    "status" => 200,
  );

  print json_encode($res);
}