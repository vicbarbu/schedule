<?php

namespace Schedule;


class ScheduleLoader
{
  private $db;
  private $user;

  public function __construct($userId) {
    $this->db = new \mysqli(DatabaseInfo::$server, DatabaseInfo::$user, DatabaseInfo::$password, DatabaseInfo::$db0);

    if($this->db->errno)
      throw new \Exception($this->db->error);

    if(!empty($userId) && !ctype_space($userId) && ctype_digit($userId)) {
      $this->user = $userId;
    } else {
      throw new \Exception("Parameters Error");
    }
  }

  public function load() {
    if($res = $this->db->query("SELECT schedule FROM account WHERE id = ".$this->user)) {
      while($row = $res->fetch_assoc()) {
        return $row["schedule"];
      }
    }
    return false;
  }
  public function save($data) {
    $sdata = json_encode($data);
    $res = $this->db->query("UPDATE account SET schedule = \"".mysql_real_escape_string($sdata)."\" WHERE id = ".$this->user);
    return $res;
  }
}