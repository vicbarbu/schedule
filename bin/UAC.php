<?php
namespace Schedule;


class UAC
{
  public static function requireLogin() {
    return isset($_SESSION['userid']) ? $_SESSION['userid'] : 0;
  }
  public static function login($email, $password) {
    $db = new \mysqli(DatabaseInfo::$server, DatabaseInfo::$user, DatabaseInfo::$password, DatabaseInfo::$db0, 2000);
    if($db->errno)
      throw new \Exception($db->error);

    if($res = $db->query("SELECT id, email, password FROM account WHERE email = '".$email."'")) {
      while($row = $res->fetch_assoc()) {
        if(password_verify($password, $row["password"])) {
          $_SESSION['userid'] = $row["id"];
          return true;
        }
      }
      return false;
    }
  }
  public static function logout() {
    unset($_SESSION['userid']);
    session_destroy();
  }
}