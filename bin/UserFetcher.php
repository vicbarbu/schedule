<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 11/3/15
 * Time: 11:23 AM
 */

namespace Schedule;


class UserFetcher
{
  private $db;
  private $userid;
  private $email;

  public function __construct($id)
  {
    $this->db = new \mysqli(DatabaseInfo::$server, DatabaseInfo::$user, DatabaseInfo::$password, DatabaseInfo::$db0);

    if ($this->db->errno)
      throw new \Exception($this->db->error);

    if ($res = $this->db->query("SELECT id, email FROM account WHERE id = " . $id)) {
      while ($row = $res->fetch_assoc()) {
        $this->email = $row["email"];
        $this->userid = $row["id"];
      }
    }
  }

  public function getUserid()
  {
    return $this->userid;
  }

  public function getEmail()
  {
    return $this->email;
  }
}