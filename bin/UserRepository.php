<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 11/3/15
 * Time: 10:43 AM
 */

namespace Schedule;

class RegisterResult {
  const SUCCESS = 0;
  const FIELDSEMPTY = 1;
  const DBERROR = 2;
  const ACCOUNTEXISTS = 3;
  const PASSWDMISMATCH = 4;
}

class UserRepository {
  private $db;

  public function __construct() {
    $this->db = new \mysqli(DatabaseInfo::$server, DatabaseInfo::$user, DatabaseInfo::$password, DatabaseInfo::$db0);

    if($this->db->errno)
      throw new \Exception($this->db->error);
  }

  public function exists($email) {
    if($stmt = $this->db->prepare("SELECT email FROM account WHERE email = ?")) {
      $stmt->bind_param("s", $email);
      $stmt->execute();

      return $stmt->num_rows > 0;
    }
    throw new \Exception($this->db->error);
  }

  public function register($email, $password1, $password2) {
    if(!ctype_space($email) && !ctype_space($password1) && !ctype_space($password2)) {
      if (!$this->exists($email)) {
        if ($stmt = $this->db->prepare("INSERT INTO account(email, password, schedule) VALUES(?, ?, \"[]\")")) {
          if ($password1 == $password2) {
            $stmt->bind_param("ss", $email, password_hash($password1, PASSWORD_BCRYPT));
            $stmt->execute();

            print $this->db->error;

            return RegisterResult::SUCCESS;
          }
          return RegisterResult::PASSWDMISMATCH;
        }
        return RegisterResult::DBERROR;
      }
      return RegisterResult::ACCOUNTEXISTS;
    }
    return RegisterResult::FIELDSEMPTY;
  }
}