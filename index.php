<?php
session_start();

require_once "bin/DatabaseInfo.php";
require_once "bin/UAC.php";
require_once "bin/UserFetcher.php";

$user = \Schedule\UAC::requireLogin();

if(!$user) {
  header("location: /user/login.html");
}

$userObject = new \Schedule\UserFetcher($user);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Schedule</title>

  <link rel="stylesheet" href="/res/style/schedule.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="/res/style/mui.css" rel="stylesheet" type="text/css" />
  <script src="//cdn.muicss.com/mui-0.2.1/js/mui.min.js"></script>
</head>
<body>

<div class="blueheader">
  <div class="container">
    <div class="logo">Schedule</div>
    <div class="spacer"></div>
    <div class="user">
      <span class="email">
        <?php
          print $userObject->getEmail();
        ?>
      </span>
      <a href="/user/logout.php" class="logout">Ieși din cont</a>
    </div>
  </div>
</div>

<div class="container">
  <div class="schedule" id="schedule"></div>
</div>

<!--<div class="pane-overlay"></div>-->
<!--<div class="pane">-->
<!--  <header class="main">-->
<!--    <i class="material-icons close">close</i>-->
<!--    Modifică orarul-->
<!--  </header>-->
<!--  <main class="parent">-->
<!--    <p>Adaugă sau șterge ore din ziua selectata din orarul tău.</p>-->
<!---->
<!--    <h2 class="group-headline">Luni</h2>-->
<!---->
<!--    <form id="add-class-form">-->
<!--      <div class="mui-select">-->
<!--        <select id="class">-->
<!--          <option value="1">Limba română</option>-->
<!--          <option value="2">Limba engleză</option>-->
<!--          <option value="3">Limba franceză</option>-->
<!--          <option value="4">Limba germană</option>-->
<!--          <option value="5">Limba latină</option>-->
<!--          <option value="6">Matematică</option>-->
<!--          <option value="7">Fizică</option>-->
<!--          <option value="8">Chimie</option>-->
<!--          <option value="9">Biologie</option>-->
<!--          <option value="10">Istorie</option>-->
<!--          <option value="11">Geografie</option>-->
<!--          <option value="12">Științe socio-umane</option>-->
<!--          <option value="13">Religie</option>-->
<!--          <option value="14">Educație muzicală</option>-->
<!--          <option value="15">Educație plastică</option>-->
<!--          <option value="16">Educație fizică</option>-->
<!--          <option value="17">Informatică</option>-->
<!--          <option value="18">T.I.C.</option>-->
<!--        </select>-->
<!--      </div>-->
<!--      <div class="inline">-->
<!--        <div class="mui-textfield">-->
<!--          <label for="add-from">De la ora</label>-->
<!--          <input type="text" placeholder="hh:mm" id="add-from" />-->
<!--        </div>-->
<!--        <div class="mui-textfield">-->
<!--          <label for="add-to">Până la ora</label>-->
<!--          <input type="text" placeholder="hh:mm" id="add-to" />-->
<!--        </div>-->
<!--      </div>-->
<!--      <div class="actions">-->
<!--        <input type="submit" class="mui-btn mui-btn--primary" value="Adaugă în orar" id="add-class" />-->
<!--      </div>-->
<!--    </form>-->
<!---->
<!--    <div class="daily-table">-->
<!--      <header>-->
<!--        <div class="time">Oră</div>-->
<!--        <div class="col">Materie</div>-->
<!--        <div class="actions"></div>-->
<!--      </header>-->
<!--      <main>-->
<!--        <div class="row">-->
<!--          <div class="time">-->
<!--            <div class="from">12:30</div>-->
<!--            <div class="to">13:20</div>-->
<!--          </div>-->
<!--          <div class="col">Matematică</div>-->
<!--          <div class="actions">-->
<!--            <i class="material-icons delete">delete</i>-->
<!--          </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--          <div class="time">-->
<!--            <div class="from">13:30</div>-->
<!--            <div class="to">14:20</div>-->
<!--          </div>-->
<!--          <div class="col">Limba română</div>-->
<!--          <div class="actions">-->
<!--            <i class="material-icons delete">delete</i>-->
<!--          </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--          <div class="time">-->
<!--            <div class="from">14:30</div>-->
<!--            <div class="to">15:20</div>-->
<!--          </div>-->
<!--          <div class="col">Educație fizică</div>-->
<!--          <div class="actions">-->
<!--            <i class="material-icons delete">delete</i>-->
<!--          </div>-->
<!--        </div>-->
<!--      </main>-->
<!--    </div>-->
<!---->
<!--    <div class="actions">-->
<!--      <input type="button" class="mui-btn" value="Anulează" />-->
<!--      <input type="button" class="mui-btn mui-btn--raised mui-btn--danger" value="Salvează" />-->
<!--    </div>-->
<!--  </main>-->
<!--</div>-->

<script src="/res/js/jquery-2.1.4.js"></script>
<script src="/res/js/home.js"></script>
</body>
</html>