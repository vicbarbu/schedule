'use strict';

function generateDOMElement(tag, attributes, content) {
  var elem = document.createElement(tag);

  if(typeof attributes === "object") {
    for(var key in attributes) {
      if(attributes.hasOwnProperty(key) && (typeof key) !== "function") {
        elem.setAttribute(key, attributes[key]);
      }
    }
  }

  if(content !== undefined) {
    elem.innerHTML = content;
  }

  return elem;
}

var coursesList = [
"Limba română",
"Limba engleză",
"Limba franceză",
"Limba germană",
"Limba latină",
"Matematică",
"Fizică",
"Chimie",
"Biologie",
"Istorie",
"Geografie",
"Științe socio-umane",
"Religie",
"Educație muzicală",
"Educație plastică",
"Educație fizică",
"Informatică",
"T.I.C."
];


var ScheduleTable = function(mountNode) {
  this._loading = true;
  this._mountNode = mountNode;
  this._data = null;

  this.courses = {
    1: "Limba română",
    2: "Limba engleză",
    3: "Limba franceză",
    4: "Limba germană",
    5: "Limba latină",
    6: "Matematică",
    7: "Fizică",
    8: "Chimie",
    9: "Biologie",
    10: "Istorie",
    11: "Geografie",
    12: "Științe socio-umane",
    13: "Religie",
    14: "Educație muzicală",
    15: "Educație plastică",
    16: "Educație fizică",
    17: "Informatică",
    18: "T.I.C."
  };

  this.render();
  $.ajax({
    url: "/ajax/load.php",
    dataType: "json",
    success: function (data) {
      this._loading = false;
      if(data.hasOwnProperty("success") && data.success === true) {
        this._data = JSON.parse(data["result"]);
        this.render();
      }
    }.bind(this)
  });
};

ScheduleTable.prototype.render = function() {
  this._mountNode.innerHTML = "";

  if(this._loading === true) {
    this._mountNode.appendChild(generateDOMElement("div", { class: "loading" }, "Se încarcă..."));
  }
  else if(this._data !== null && typeof this._data === "object") {
    var days = [ "Luni", "Marți", "Miercuri", "Joi", "Vineri" ];
    var maxCoursesCount = 0;

    for(var i = 0; i < this._data.length; i++) {
      if(this._data[i].length > maxCoursesCount) maxCoursesCount = this._data[i].length;
    }

    for(var dayIndex = 0; dayIndex < 5; dayIndex++) {
      var dayDOM = generateDOMElement("div", { class: "day" });
      dayDOM.dataset.index = dayIndex;

      // append day name
      dayDOM.appendChild(generateDOMElement("header", {}, days[dayIndex]));

      // append rows for each day
      for(var courseIndex = 0; courseIndex < maxCoursesCount; courseIndex++) {
        var courseRowDOM = generateDOMElement("div", {class: "course"});

        // if the course really exists, add it's name
        if(this._data.hasOwnProperty(dayIndex) && this._data[dayIndex].hasOwnProperty(courseIndex)) {
          courseRowDOM.innerHTML = this.courses[this._data[dayIndex][courseIndex]["class"]];
        }
        // append row
        dayDOM.appendChild(courseRowDOM);
      }

      // append day to table
      this._mountNode.appendChild(dayDOM);
    }
  }

};


var SetupDayPane = function(day) {
  this._day = day;
  this._loading = true;
  this.days = [ "Luni", "Marți", "Miercuri", "Joi", "Vineri" ];
  this._currentCourseId = 0;

  this.currentCourses = [];

  $.ajax({
    url: "/ajax/load.php",
    dataType: "json",
    success: function (data) {
      this._loading = false;
      if(data.hasOwnProperty("success") && data.success === true) {
        this.data = JSON.parse(data["result"]);
        this.render();
      }
    }.bind(this)
  });
};

SetupDayPane.prototype._renderHeader = function(mountNode, closeCallback) {
  var header = generateDOMElement("header", {class: "main"}, "Modifică orarul");
  var closeButton = generateDOMElement("i", {class: "material-icons close"}, "close");
  closeButton.addEventListener("click", closeCallback);

  header.appendChild(closeButton);

  mountNode.appendChild(header);
};
SetupDayPane.prototype.generateTable = function(data) {
  var table = generateDOMElement("div", {class: "daily-table"});
  var tableHeader = generateDOMElement("header", {}, "Materie");

  table.appendChild(tableHeader);

  if(typeof data === "object" && data.hasOwnProperty(this._day)) {
    for(var courseIndex in data[this._day]) {
      if(data[this._day].hasOwnProperty(courseIndex)) {
        /*var row = document.createElement("div");
        row.classList.add("row");
        row.dataset.id = this._currentCourseId;

        var courseCaption = document.createElement("div");
        courseCaption.classList.add("col");
        courseCaption.innerHTML = coursesList[data[this._day][courseIndex]["class"] - 1];

        var actions = generateDOMElement("div", {class: "actions"});
        actions.appendChild(generateDOMElement("i", {class:"material-icons"}, "delete"));

        row.appendChild(courseCaption);
        row.appendChild(actions);

        table.appendChild(row);

        $(table).on("click", ".material-icons", function(e) {
          var uniqueId = $(this).parent().parent().data("id");

          $(table).find(".row[data-id=\"" + uniqueId + "\"]").remove();
        });

        this._currentCourseId++;*/
        this.appendCourseToTable(table, data[this._day][courseIndex]["class"]);
      }
    }
  }

  return table;
};
SetupDayPane.prototype.appendCourseToTable = function(table, courseId) {
  var row = document.createElement("div");
  row.classList.add("row");
  row.dataset.id = this._currentCourseId;

  var courseCaption = document.createElement("div");
  courseCaption.classList.add("col");
  courseCaption.innerHTML = coursesList[courseId - 1];

  var actions = generateDOMElement("div", {class: "actions"});
  actions.appendChild(generateDOMElement("i", {class:"material-icons"}, "delete"));

  row.appendChild(courseCaption);
  row.appendChild(actions);

  table.appendChild(row);

  var that = this;
  $(table).on("click", ".material-icons", function(e) {
    var uniqueId = $(this).parent().parent().data("id");

    $(table).find(".row[data-id=\"" + uniqueId + "\"]").remove();

    delete that.currentCourses[uniqueId];
  });

  this.currentCourses[this._currentCourseId] = { class: courseId };
  this._currentCourseId++;
};

SetupDayPane.prototype.render = function() {
  var paneOverlay = generateDOMElement("div", { class: "pane-overlay"});
  var pane = generateDOMElement("div", {class: "pane"});
  var paddingContainer = generateDOMElement("main", {class: "parent"});

  this._renderHeader(pane, function() {
    paneOverlay.remove();
    pane.remove();
  });

  paddingContainer.appendChild(generateDOMElement("h2", {class: "group-headline"}, this.days[this._day]));

  if(!this._loading) {
    // render the form
    var form = generateDOMElement("form", {id: "add-class-form"});
    var muiSelect = generateDOMElement("div", {class:"mui-select"});
    var cmpSelect = generateDOMElement("select", {id: "course-selector"});

    var table = this.generateTable(this.data);

    for(var i = 0; i <= coursesList.length; i++) {
      cmpSelect.appendChild(generateDOMElement("option", {value: i+1}, coursesList[i]));
    }

    // append select component to its div
    muiSelect.appendChild(cmpSelect);

    // generate the div for the action buttons
    var actionsDiv = generateDOMElement("div", {class: "actions"});

    // generate the primary button
    var addClassBtn = generateDOMElement("input", {
      type: "submit",
      class: "mui-btn mui-btn--primary",
      id: "add-class",
      value: "Adaugă în orar"
    });
    actionsDiv.appendChild(addClassBtn);

    // append everything to form
    form.appendChild(muiSelect);
    form.appendChild(actionsDiv);

    var that = this;
    form.addEventListener("submit", function(e) {
      e.preventDefault();

      that.appendCourseToTable(table, document.getElementById("course-selector").value);
    });

    var paneActions = generateDOMElement("div", {class:"actions"});
    var cancelButton = generateDOMElement("input", {
      type: "button",
      class: "mui-btn",
      value: "anulează"
    });
    cancelButton.addEventListener("click", function(e) {
      paneOverlay.remove();
      pane.remove();
    });
    var saveButton = generateDOMElement("input", {
      type: "button",
      class: "mui-btn mui-btn--raised mui-btn--danger",
      value: "salvează"
    });

    saveButton.addEventListener("click", function() {
      // get the current user data
      $.ajax({
        url: "/ajax/load.php",
        dataType: "json",
        success: function (data) {
          if(data.hasOwnProperty("success") && data.success === true) {
            var currentData = JSON.parse(data["result"]);
            currentData[that._day] = that.currentCourses;


            $.ajax({
              url: "/ajax/save.php",
              dataType: "json",
              method: "POST",
              data: {
                data: currentData
              },
              success: function() {
                paneOverlay.remove();
                pane.remove();

                var mountNode = document.getElementById("schedule");
                mountNode.innerHTML = "";

                new ScheduleTable(mountNode);
              }
            })
          }
        }.bind(this)
      });
    });


    paneActions.appendChild(cancelButton);
    paneActions.appendChild(saveButton);


    // append the form to the mount node
    paddingContainer.appendChild(form);
    paddingContainer.appendChild(table);
    paddingContainer.appendChild(paneActions);

    pane.appendChild(paddingContainer);
  } else {
    pane.appendChild(generateDOMElement("div", {class: "loading"}, "Se încarcă..."));
  }

  document.body.appendChild(paneOverlay);
  document.body.appendChild(pane);
};

// document load handler

$(function() {
  new ScheduleTable(document.getElementById("schedule"));

  $(document.body).on("click", ".schedule .day", function(e) {
    var dayIndex = $(this).data("index");

    new SetupDayPane(dayIndex);
  })
});