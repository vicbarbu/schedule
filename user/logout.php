<?php
session_start();
require_once "../bin/DatabaseInfo.php";
require_once "../bin/UAC.php";

\Schedule\UAC::logout();

header("location: /");
?>